I was one of those kids that enjoyed creating "hacks" for multiplayer and "trolling" people with them. Dark times...

This is a script that reads the chat messages from other users and repeats it as yourself. Don't remember if it works or not.

How to use:
Press Right-CTRL + T to toggle the mod.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
