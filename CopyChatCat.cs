﻿using GTA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CopyChatCat
{
    public class CopyChatCat : Script
    {
        private bool enabled = false;

        public CopyChatCat()
        {
            KeyDown += CopyChatCat_KeyDown;
            while (true)
            {
                DoTick();
                Wait(0);
            }
        }

        void CopyChatCat_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(Keys.RControlKey) && e.Key == Keys.T)
            {
                enabled = !enabled;
                Subtitle("CopyChatCat is now " + (enabled ? "enabled" : "disabled"));
            }
        }

        private void DoTick()
        {
            if (enabled && Game.isMultiplayer)
            {
                string textChat = NetworkGetNextTextChat();
                int textChatPlayerId = NetworkGetPlayerIdOfNextTextChat();

                if (textChat != "" && textChatPlayerId != -1)
                {
                    if (textChatPlayerId != Player.ID)
                    {
                        Game.SendChatMessage(textChat);
                    }
                    string temp = "Last chat message: \"" + textChat + "\", last chat message plr id: \"" + textChatPlayerId.ToString() + "\"";
                    Game.DisplayText(temp);
                    Game.Console.Print(temp);
                }
            }
        }

        private int NetworkGetPlayerIdOfNextTextChat()
        {
            return GTA.Native.Function.Call<int>("NETWORK_GET_PLAYER_ID_OF_NEXT_TEXT_CHAT");
        }

        private string NetworkGetNextTextChat()
        {
            return GTA.Native.Function.Call<string>("NETWORK_GET_NEXT_TEXT_CHAT");
        }

        private void Subtitle(string text, int time = 2000)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", text, time, 1);
        }
    }
}
